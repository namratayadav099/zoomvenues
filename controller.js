angular
    .module('firstApp')
    .controller('MyController',MyController)

    MyController.$inject = ['$scope','$http'];
    
    function MyController($scope, $http){
        $scope.Users = [];
        $http.get('http://jsonplaceholder.typicode.com/users')
        .then(function(response){
            $scope.Users = response.data;
        }, function(error){
            console.log(error);
        });
    }

